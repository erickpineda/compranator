﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System;
using Compranator.Helpers;
using System.Globalization;

namespace Compranator.Models
{
    public class Cliente : IdentityUser
    {
        public async Task<ClaimsIdentity>
            GenerateUserIdentityAsync(UserManager<Cliente> manager)
        {
            var userIdentity = await manager
                .CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        [Display(Name = "Correo postal")]
        public string correo_postal { get; set; }

        [Display(Name = "Municipio")]
        public string municipio { get; set; }

        [Display(Name = "Provincia")]
        public string provincia { get; set; }

        [Display(Name = "Código postal")]
        [RegularExpression(@"[0-9]{5}")]
        public string codigo_postal { get; set; }

        [Display(Name = "Cumpleaños")]
        [DataType(DataType.Date)]
        public DateTime fecha_nacimiento { get; set; }

        [Display(Name = "Teléfono 1")]
        [RegularExpression(@"(6|9)[0-9]{7}\d")]
        public string telefono1 { get; set; }

        [Display(Name = "Teléfono 2")]
        [RegularExpression(@"(6|9)[0-9]{7}\d")]
        public string telefono2 { get; set; }

        public ICollection<Pedido> PedidosClientes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            //throw new NotImplementedException();
            if (ComprobarEdad.MayorDe18(fecha_nacimiento) < 18)
            {
                yield return new ValidationResult
              ("La fecha de nacimiento no es correcta ", new[] { "fecha_nacimiento" });
            }
        }

        // Concatena la información del usuario para mostrarlo en la tabla
        [Display(Name = "Información")]
        public string info_usuario
        {
            get
            {
                string dspDireccionPostal = string.IsNullOrWhiteSpace(this.correo_postal) ? "" : this.correo_postal;
                string dspMunicipio = string.IsNullOrWhiteSpace(this.municipio) ? "" : this.municipio;
                string dspProvincia = string.IsNullOrWhiteSpace(this.provincia) ? "" : this.provincia;
                string dspCodigoPostal = string.IsNullOrWhiteSpace(this.codigo_postal) ? "" : this.codigo_postal;
                return string.Format("{0} {1} {2} {3}", dspDireccionPostal, dspMunicipio, dspProvincia, dspCodigoPostal);
            }
        }
    }

    public class AppRol : IdentityRole
    {
        public AppRol() : base() { }
        public AppRol(string name) : base(name) { }

        [Display(Name = "Descripción")]
        public string descripcion { get; set; }

    }

    /*public class Cliente : User
    {
        [Display(Name = "Cumpleaños")]
        [DataType(DataType.Date)]
        public DateTime fecha_nacimiento { get; set; }

        [Display(Name = "Teléfono 1")]
        [RegularExpression(@"(6|9)[0-9]{7}\d")]
        public string telefono1 { get; set; }

        [Display(Name = "Teléfono 2")]
        [RegularExpression(@"(6|9)[0-9]{7}\d")]
        public string telefono2 { get; set; }

        public virtual ICollection<Pedido> PedidosClientes { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //throw new NotImplementedException();
            if (fecha_nacimiento > DateTime.Now)
            {
                yield return new ValidationResult
              ("La fecha de nacimiento no es correcta ", new[] { "fecha_nacimiento" });
            }
        }
    }*/

    /*public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }*/
}