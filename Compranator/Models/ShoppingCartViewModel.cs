﻿using Compranator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compranator.Models
{
    public class ShoppingCartViewModel
    {
        public List<Carrito> ProductosEnCarrito { get; set; }
        public decimal total { get; set; }
    }

    public class ShoppingCartRemoveViewModel
    {
        public string mensaje { get; set; }
        public decimal total { get; set; }
        public int cartCount { get; set; }
        public int productoCount { get; set; }
        public int borrarId { get; set; }
    }
}