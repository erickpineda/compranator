﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Compranator.Models
{
    public class Carrito
    {
        // [Key]
        public virtual int ID { get; set; }

        public virtual string CarritoId { get; set; }

        public virtual int cantidad { get; set; }

        public virtual System.DateTime fecha_carrito { get; set; }

        public virtual Pedido PedidoCarrito { get; set; }//este no

        public virtual int ProductoCarritoFK { get; set; }

        public virtual Producto ProductoCarrito { get; set; }
    }
}