﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Compranator.Models
{
    public class CompranatorContexto : IdentityDbContext<Cliente>
    {
        // Forzar conexion con la base de datos, indicando el 
        // nombre del ConnectionString del web.config
        public CompranatorContexto()
            : base("CompranatorContexto", throwIfV1Schema: false)
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
        }

        /*static CompranatorContexto()
        {
            Database.SetInitializer<CompranatorContexto>(new ApplicationDbInitializer());
        }*/

        public static CompranatorContexto Create()
        {
            return new CompranatorContexto();
        }

        //public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Pedido> Pedidos { get; set; }
        public DbSet<Carrito> Carritos { get; set; }
        public DbSet<Detalle> Detalles { get; set; }
        //public DbSet<Modo_Envio> ModosEnvio { get; set; }
        public DbSet<Modo_Pago> ModosPago { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Producto>()
                .HasRequired(p => p.Categoria)
                .WithMany(c => c.Productos)
                .HasForeignKey(p => p.CategoriaFK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Carrito>()
                .HasRequired(c => c.ProductoCarrito)
                .WithMany(p => p.CarritoEnProductos)
                .HasForeignKey(c => c.ProductoCarritoFK)
                .WillCascadeOnDelete(false);

            /*modelBuilder.Entity<Pedido_Producto>()
                .HasRequired(p => p.Producto)
                .WithMany(p => p.ProductosPedidos)
                .HasForeignKey(p => p.ProductoFK)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Pedido_Producto>()
                .HasRequired(p => p.Pedido)
                .WithMany(p => p.Carritos)
                .HasForeignKey(p => p.PedidoFK)
                .WillCascadeOnDelete(true);*/

            modelBuilder.Entity<Pedido>()
                .HasRequired(p => p.Cliente)
                .WithMany(c => c.PedidosClientes)
                .HasForeignKey(p => p.ClienteFK)
                .WillCascadeOnDelete(false);

            /*modelBuilder.Entity<Pedido>()
                .HasRequired(p => p.Modo_Envio)
                .WithMany(me => me.PedidosEnvio)
                .HasForeignKey(p => p.Modo_EnvioFK)
                .WillCascadeOnDelete(false);*/

            modelBuilder.Entity<Pedido>()
                .HasRequired(p => p.Modo_Pago)
                .WithMany(mp => mp.PedidosPago)
                .HasForeignKey(p => p.Modo_PagoFK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Detalle>()
                .HasRequired(d => d.PedidoEnDetalle)
                .WithMany(p => p.DetallesPedido)
                .HasForeignKey(d => d.PedidoEnDetalleFK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Pedido>()
                .HasKey(p => p.CarritoPedidoFK);

            modelBuilder.Entity<Carrito>()
                .HasRequired(c => c.PedidoCarrito)
                .WithRequiredPrincipal(p => p.CarritoPedido);

        }
    }
}