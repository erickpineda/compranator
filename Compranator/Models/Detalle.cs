﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó mediante una herramienta.
//     Los cambios del archivo se perderán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Compranator.Models
{
    public class Detalle
    {
        public virtual int ID { get; set; }
        public virtual System.DateTime fecha { get; set; }

        public virtual string descripcion { get; set; }

        public virtual string numeroFactura { get; set; }

        public virtual int PedidoEnDetalleFK { get; set; }

        public virtual int ProductoEnDetalleFK { get; set; }

        public virtual Producto ProductoEnDetalle { get; set; }

        public virtual int cantidad { get; set; }

        [Range(0.00, 100000.00,
            ErrorMessage = "El precio debe estar entre 0.00 y 100000.00")]
        public virtual decimal precio_unitario { get; set; }

        public virtual int iva { get; set; }

        [Required(ErrorMessage = "El precio es obligatorio")]
        [Range(0.00, 100000.00,
            ErrorMessage = "El precio debe estar entre 0.00 y 100000.00")]
        public virtual decimal total { get; set; }

        public virtual Pedido PedidoEnDetalle { get; set; }

    }
}