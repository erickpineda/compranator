﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Compranator.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Nombre del rol")]
        public string nombre { get; set; }
        [Display(Name = "Descripción")]
        public string descripcion { get; set; }
    }

    public class EditUserViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Display(Name = "Correo postal")]
        public string correo_postal { get; set; }

        [Display(Name = "Municipio")]
        public string municipio { get; set; }

        [Display(Name = "Provincia")]
        public string provincia { get; set; }

        // Use a sensible display name for views:
        [Display(Name = "Código postal")]
        [RegularExpression(@"[0-9]{5}")]
        public string codigo_postal { get; set; }

        [Display(Name = "Cumpleaños")]
        [DataType(DataType.Date)]
        public DateTime fecha_nacimiento { get; set; }

        [Display(Name = "Teléfono 1")]
        [RegularExpression(@"(6|9)[0-9]{7}\d")]
        public string telefono1 { get; set; }

        [Display(Name = "Teléfono 2")]
        [RegularExpression(@"(6|9)[0-9]{7}\d")]
        public string telefono2 { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}