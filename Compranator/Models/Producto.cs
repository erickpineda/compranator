﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó mediante una herramienta.
//     Los cambios del archivo se perderán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Compranator.Models
{
    public class Producto
    {
        public string modelo { get; set; }

        public int ID { get; set; }

        public string color { get; set; }

        public string talla { get; set; }

        public string tipo_talla { get; set; }

        public string sistemaDeTallas { get; set; }

        public string edades { get; set; }

        public string genero { get; set; }

        public string marca { get; set; }

        public bool stock { get; set; }

        [Required(ErrorMessage = "El precio es obligatorio")]
        [Range(0.00, 100000.00,
            ErrorMessage = "El precio debe estar entre 0.00 y 100000.00")]
        public decimal precio { get; set; }

        public string descripcion { get; set; }

        public string url_image { get; set; }

        public int CategoriaFK { get; set; }

        public Categoria Categoria { get; set; }

        // No tendra esta colección para cuando agregue el carrito
        //public virtual ICollection<Pedido_Producto> ProductosPedidos { get; set; }
        
        
        //falta coleccion de detalles
        //public virtual List<Detalle> Detalles { get; set; }

        public ICollection<Carrito> CarritoEnProductos { get; set; }//este no
    }
}