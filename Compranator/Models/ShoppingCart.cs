﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compranator.Models
{
    public partial class ShoppingCart
    {
        CompranatorContexto db = new CompranatorContexto();
        string ShoppingCartId { get; set; }
        public const string CartSessionKey = "CarritoId";

        public static ShoppingCart GetCart(HttpContextBase context)
        {
            var cart = new ShoppingCart();
            cart.ShoppingCartId = cart.GetCartId(context);
            return cart;
        }

        // Método que simplifica la llamada de los carritos
        public static ShoppingCart GetCart(Controller controller)
        {
            return GetCart(controller.HttpContext);
        }
        public int AddToCart(Producto producto)
        {
            var cartItem = db.Carritos.SingleOrDefault(
                c => c.CarritoId == ShoppingCartId
                && c.ProductoCarritoFK == producto.ID);

            if (cartItem == null)
            {
                // Crea un nuevo carrito para el producto, si no existe
                cartItem = new Carrito
                {
                    ProductoCarritoFK = producto.ID,
                    CarritoId = ShoppingCartId,
                    cantidad = 1,
                    fecha_carrito = DateTime.Now
                };
                db.Carritos.Add(cartItem);
            }
            else
            {
                // Si existe un mismo producto en el carrito, 
                // incrementa la cantidad
                cartItem.cantidad++;
            }
            // Persistir en la BDD
            db.SaveChanges();
            return cartItem.cantidad;
        }

        public int RemoveFromCart(int id)
        {
            // Traer el carrito
            var cartItem = db.Carritos.Single(
                cart => cart.CarritoId == ShoppingCartId
                && cart.ID == id);

            int itemCount = 0;

            if (cartItem != null)
            {
                if (cartItem.cantidad > 1)
                {
                    cartItem.cantidad--;
                    itemCount = cartItem.cantidad;
                }
                else
                {
                    db.Carritos.Remove(cartItem);
                }
                // Persistir en la BDD
                db.SaveChanges();
            }
            return itemCount;
        }

        public void EmptyCart()
        {
            var cartItems = db.Carritos.Where(
                cart => cart.CarritoId == ShoppingCartId);

            foreach (var cartItem in cartItems)
            {
                db.Carritos.Remove(cartItem);
            }
            // Persistir en la BDD
            db.SaveChanges();
        }

        public List<Carrito> GetCartItems()
        {
            return db.Carritos.Where(
                cart => cart.CarritoId == ShoppingCartId).ToList();
        }

        public int GetCount()
        {
            // Obtener el recuento de cada artículo en el carro y sumarlos
            int? count = (from cartItems in db.Carritos
                          where cartItems.CarritoId == ShoppingCartId
                          select (int?)cartItems.cantidad).Sum();
            // Regreso 0 si todas las entradas son nulas
            return count ?? 0;
        }

        public decimal GetTotal()
        {
            // Multiply product price by count of that product to get 
            // the current price for each of those products in the cart
            // sum all product price totals to get the cart total
            decimal? total = (from cartItems in db.Carritos
                              where cartItems.CarritoId == ShoppingCartId
                              select (int?)cartItems.cantidad *
                              cartItems.ProductoCarrito.precio).Sum();

            return total ?? decimal.Zero;
        }

        public Pedido CreateOrder(Pedido pedido)
        {
            decimal orderTotal = 0;
            pedido.DetallesPedido = new List<Detalle>();

            var cartItems = GetCartItems();
            // Iterar sobre los elementos en la compra, 
            // añadiendo los detalles de la orden para cada
            foreach (var item in cartItems)
            {
                var orderDetail = new Detalle
                {
                    ProductoEnDetalleFK = item.ProductoCarritoFK,
                    PedidoEnDetalleFK = pedido.ID,
                    precio_unitario = item.ProductoCarrito.precio,
                    cantidad = item.cantidad
                };
                // Ajuste el total del pedido de la cesta de la compra
                orderTotal += (item.cantidad * item.ProductoCarrito.precio);
                pedido.DetallesPedido.Add(orderDetail);
                db.Detalles.Add(orderDetail);

            }
            // Ajuste total del fin de la cuenta OrderTotal
            pedido.total = orderTotal;

            // Persistir el pedido en la BDD
            db.SaveChanges();
            // Vaciar el carrito de la compra
            EmptyCart();
            // Retornar el ID del predido para confirmar la compra
            return pedido;
        }

        // We're using HttpContextBase to allow access to cookies.
        public string GetCartId(HttpContextBase context)
        {
            if (context.Session[CartSessionKey] == null)
            {
                if (!string.IsNullOrWhiteSpace(context.User.Identity.Name))
                {
                    context.Session[CartSessionKey] =
                        context.User.Identity.Name;
                }
                else
                {
                    // Generar un nuevo GUID al azar usando la clase System.Guid
                    Guid tempCartId = Guid.NewGuid();
                    // Enviar tempCartId nuevo a cliente como una cookie
                    context.Session[CartSessionKey] = tempCartId.ToString();
                }
            }
            return context.Session[CartSessionKey].ToString();
        }
        // When a user has logged in, migrate their shopping cart to
        // be associated with their username
        public void MigrateCart(string userName)
        {
            var shoppingCart = db.Carritos.Where(
                c => c.CarritoId == ShoppingCartId);

            foreach (Carrito item in shoppingCart)
            {
                item.CarritoId = userName;
            }
            db.SaveChanges();
        }
    }
}