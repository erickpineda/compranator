﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using System.Globalization;

namespace Compranator.Models
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

    public class AdministradorDeUsuarios : UserManager<Cliente>
    {
        public AdministradorDeUsuarios(IUserStore<Cliente> store)
            : base(store)
        {
        }

        public static AdministradorDeUsuarios Create(IdentityFactoryOptions<AdministradorDeUsuarios> options,
            IOwinContext context)
        {
            var manager = new AdministradorDeUsuarios(new UserStore<Cliente>(context.Get<CompranatorContexto>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<Cliente>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };
            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;
            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug in here.
            manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<Cliente>
            {
                MessageFormat = "Tu código de seguridad es: {0}"
            });
            manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<Cliente>
            {
                Subject = "SecurityCode",
                BodyFormat = "Tu código de seguridad es: {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<Cliente>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the RoleManager used in the application. RoleManager is defined in the ASP.NET Identity core assembly
    public class AdministradorDeRoles : RoleManager<AppRol>
    {
        public AdministradorDeRoles(IRoleStore<AppRol, string> roleStore)
            : base(roleStore)
        {
        }

        public static AdministradorDeRoles Create(IdentityFactoryOptions<AdministradorDeRoles> options, IOwinContext context)
        {
            return new AdministradorDeRoles(new RoleStore<AppRol>(context.Get<CompranatorContexto>()));
        }
    }

    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your sms service here to send a text message.
            return Task.FromResult(0);
        }
    }

    // This is useful if you do not want to tear down the database each time you run the application.
    // public class ApplicationDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    // This example shows you how to create a new database if the Model changes
    public class ApplicationDbInitializer : DropCreateDatabaseIfModelChanges<CompranatorContexto>
    {
        protected override void Seed(CompranatorContexto context)
        {
            InitializeIdentityForEF(context);
            base.Seed(context);
        }

        //Create User=Admin@Admin.com with password=Admin@123456 in the Admin role        
        public static void InitializeIdentityForEF(CompranatorContexto db)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<AdministradorDeUsuarios>();
            var roleManager = HttpContext.Current.GetOwinContext().Get<AdministradorDeRoles>();
            const string name = "admin@compranator.com";
            const string password = "Admin@123456";
            const string roleName = "Admin";

            //Create Role Admin if it does not exist
            var role = roleManager.FindByName(roleName);
            if (role == null)
            {
                role = new AppRol(roleName);
                var roleresult = roleManager.Create(role);
            }

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new Cliente { UserName = name, Email = name };
                user.fecha_nacimiento = DateTime.ParseExact("1990-03-03", "yyyy-MM-dd", CultureInfo.InvariantCulture);
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);
            }

            // Agrega el usuario Admin al rol creado si aún no lo ha agregado
            var rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(role.Name))
            {
                var result = userManager.AddToRole(user.Id, role.Name);
            }
        }
    }

    public class ApplicationSignInManager : SignInManager<Cliente, string>
    {
        public ApplicationSignInManager(AdministradorDeUsuarios userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager) { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(Cliente user)
        {
            return user.GenerateUserIdentityAsync((AdministradorDeUsuarios)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<AdministradorDeUsuarios>(), context.Authentication);
        }
    }
}