﻿using Compranator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Compranator.Helpers
{
    public static class UsersHelpers
    {
        public static Cliente GetCurrentUser(Controller c, CompranatorContexto db)
        {
            var username = c.User.Identity.Name;
            return db.Users.Where(x => x.UserName == username).First();
        }
    }

    public static class ComprobarEdad
    {
        public static string RetornarMensajeCumple(DateTime cumple)
        {
            int diaCumple = cumple.Day; // Dia del Cumpleanios
            int mesCumple = cumple.Month; // Mes de Cumple 4=Abril
            int anioCumple = cumple.Year; // Año de Cumple
            DateTime fechaNacimiento = new DateTime(anioCumple, mesCumple, diaCumple);

            //Se calcula la Edad Actual A partir de la fecha actual Sustrayendo la fecha de nacimiento
            //esto devuelve un TimeSpan por tanto tomaremos los Dias y lo dividimos en 365 días
            int edad = (DateTime.Now.Subtract(fechaNacimiento).Days / 365);

            DateTime proximoCumple;
            //Define el proximo Cumple, En caso de que el mes sea menor al Mes Actual se busca el Próxima fecha que seria del año que viene
            //es por ello el AddYear(1)
            //En caso de ser mayor se toma el año actual
            if (DateTime.Now.Month <= mesCumple && DateTime.Now.Day <= diaCumple)
                proximoCumple = new DateTime(DateTime.Now.AddYears(1).Year, mesCumple, diaCumple);
            else
                proximoCumple = new DateTime(DateTime.Now.Year, mesCumple, diaCumple);


            //Definiremos los dias faltantes para el proximo cumple
            TimeSpan faltan = proximoCumple.Subtract(DateTime.Now);

            //Ahora Elaboramos el Mensaje
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Usted Tiene {0} Años ", edad);
            sb.AppendFormat("y tu Próximo Cumpleaños es: {0} Días", faltan.Days);
            sb.AppendFormat(", {0} Horas ", faltan.Hours);
            sb.AppendFormat("y {0} Minutos ", faltan.Minutes);

            return sb.ToString();
        }

        public static int MayorDe18(DateTime birthday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - birthday.Year;
            if (now < birthday.AddYears(age)) age--;

            return age;
        }
    }
}