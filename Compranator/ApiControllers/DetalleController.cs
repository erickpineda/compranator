﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Compranator.Models;

namespace Compranator.ApiControllers
{
    public class DetalleController : ApiController
    {
        private CompranatorContexto db = new CompranatorContexto();

        // GET api/Detalle
        public IQueryable<Detalle> GetDetalles()
        {
            return db.Detalles;
        }

        // GET api/Detalle/5
        [ResponseType(typeof(Detalle))]
        public IHttpActionResult GetDetalle(int id)
        {
            Detalle detalle = db.Detalles.Find(id);
            if (detalle == null)
            {
                return NotFound();
            }

            return Ok(detalle);
        }

        // PUT api/Detalle/5
        public IHttpActionResult PutDetalle(int id, Detalle detalle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detalle.ID)
            {
                return BadRequest();
            }

            db.Entry(detalle).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetalleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Detalle
        [ResponseType(typeof(Detalle))]
        public IHttpActionResult PostDetalle(Detalle detalle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Detalles.Add(detalle);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = detalle.ID }, detalle);
        }

        // DELETE api/Detalle/5
        [ResponseType(typeof(Detalle))]
        public IHttpActionResult DeleteDetalle(int id)
        {
            Detalle detalle = db.Detalles.Find(id);
            if (detalle == null)
            {
                return NotFound();
            }

            db.Detalles.Remove(detalle);
            db.SaveChanges();

            return Ok(detalle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DetalleExists(int id)
        {
            return db.Detalles.Count(e => e.ID == id) > 0;
        }
    }
}