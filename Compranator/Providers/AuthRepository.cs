﻿using Compranator.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Compranator.Providers
{
    public class AuthRepository : IDisposable
    {
        private CompranatorContexto _ctx;

        private UserManager<Cliente> _userManager;

        public AuthRepository()
        {
            _ctx = new CompranatorContexto();
            _userManager = new UserManager<Cliente>(new UserStore<Cliente>(_ctx));
        }

        public async Task<IdentityResult> RegisterUser(Cliente userModel)
        {
            Cliente user = new Cliente
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.PasswordHash);

            return result;
        }

        public async Task<Cliente> FindUser(string userName, string password)
        {
            Cliente user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}