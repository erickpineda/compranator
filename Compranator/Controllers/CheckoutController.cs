﻿using Compranator.Helpers;
using Compranator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compranator.Controllers
{
    [Authorize(Roles = "Cliente")]
    public class CheckoutController : Controller
    {
        CompranatorContexto db = new CompranatorContexto();
        const string PromoCode = "FREE";
        //
        // GET: /Checkout/
        public ActionResult AddressAndPayment()
        {
            var currentUser = UsersHelpers.GetCurrentUser(this, db);
            var order = new Pedido();
            var cart = ShoppingCart.GetCart(this.HttpContext);
            order.total = cart.GetTotal();
            order.fecha_pedido = DateTime.Now;
            order.ClienteFK = currentUser.Id;

            return View(order);
        }

        //
        // POST: /Checkout/AddressAndPayment
        [HttpPost]
        public ActionResult AddressAndPayment(FormCollection values)
        {
            var currentUser = UsersHelpers.GetCurrentUser(this, db);
            var order = new Pedido();
            TryUpdateModel(order);

            try
            {
                if (string.Equals(values["PromoCode"], PromoCode,
                    StringComparison.OrdinalIgnoreCase) == false)
                {
                    return View(order);
                }
                else
                {
                    order.Cliente.UserName = currentUser.UserName;//User.Identity.Name;
                    order.fecha_pedido = DateTime.Now;
                    order.ClienteFK = currentUser.Id;

                    // Guardar el pedido
                    db.Pedidos.Add(order);
                    db.SaveChanges();
                    // Procesar el pedido
                    var cart = ShoppingCart.GetCart(this.HttpContext);
                    cart.CreateOrder(order);

                    return RedirectToAction("Complete",
                        new { id = order.ID });
                }
            }
            catch
            {
                //Invalid - redisplay with errors
                return View(order);
            }
        }

        //
        // GET: /Checkout/Complete
        public ActionResult Complete(int id)
        {
            // Validate customer owns this order
            bool isValid = db.Pedidos.Any(
                o => o.ID == id &&
                o.Cliente.UserName == User.Identity.Name);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }
	}
}