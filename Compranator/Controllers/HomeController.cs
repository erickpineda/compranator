﻿using Compranator.Models;
using System.Web.Mvc;

namespace Compranator.Controllers
{
    public class HomeController : Controller
    {
        private CompranatorContexto db = new CompranatorContexto();
        public ActionResult Index()
        {
            var productos = db.Productos.Include("Categoria");
            return View(productos);
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Descripción.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Información";

            return View();
        }
    }
}
