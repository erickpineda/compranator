﻿using Compranator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compranator.Controllers
{
    public class ShoppingCartController : Controller
    {
        CompranatorContexto db = new CompranatorContexto();
        //
        // GET: /ShoppingCart/
        public ActionResult Index()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Mostrará el contenido del carrito a partir del ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                ProductosEnCarrito = cart.GetCartItems(),
                total = cart.GetTotal()
            };
            // Retorna a la vista del carrito - Index -
            return View(viewModel);
        }

        //
        // GET: /ShoppingCart/AddToCart/5
        public ActionResult AddToCart(int id)
        {
            // Rocoge el producto de la BDD
            var agregarProducto = db.Productos
                .Single(prod => prod.ID == id);

            // Agrega el producto al carrito
            var cart = ShoppingCart.GetCart(this.HttpContext);

            int count = cart.AddToCart(agregarProducto);

            var resultado = new ShoppingCartRemoveViewModel
            {
                mensaje = Server.HtmlEncode(agregarProducto.modelo) +
                " ha sido agregado al carrito.",
                total = cart.GetTotal(),
                cartCount = cart.GetCount(),
                productoCount = count,
                borrarId = id
            };

            // Vuelve a los productos después de comprar
            //return Json(resultado);
            return RedirectToAction("Index");
           
        }

        //
        // AJAX: /ShoppingCart/RemoveFromCart/5
        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            // Recoge el producto para remover del carrito
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Recoge el nombre del producto para mostrarlo al comprar
            string nombreProducto = db.Carritos
                .Single(item => item.ID == id).ProductoCarrito.modelo;

            // Remueve un producto del carrito
            int itemCount = cart.RemoveFromCart(id);

            // Muestra un mensaje de confirmación
            var results = new ShoppingCartRemoveViewModel
            {
                mensaje = "Un (1)" + Server.HtmlEncode(nombreProducto) +
                    " se ha removido del carrito.",

                total = cart.GetTotal(),
                cartCount = cart.GetCount(),
                productoCount = itemCount,
                borrarId = id
            };
            return Json(results);
        }

        //
        // GET: /ShoppingCart/CartSummary
        [ChildActionOnly]
        public ActionResult CartSummary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            ViewData["cartCount"] = cart.GetCount();
            return PartialView("CartSummary");
        }

        public void vaciarCarrito()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);
            cart.EmptyCart();
        }
    }
    public class AllowJsonGetAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var jsonResult = filterContext.Result as JsonResult;

            if (jsonResult == null)
                throw new ArgumentException("Action does not return a JsonResult, attribute AllowJsonGet is not allowed");

            jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;

            base.OnResultExecuting(filterContext);
        }
    }
}