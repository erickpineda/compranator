﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Compranator.Models;

namespace Compranator.Controllers
{
    public class PedidoController : Controller
    {
        private CompranatorContexto db = new CompranatorContexto();

        // GET: /Pedido/
        public ActionResult Index()
        {
            var pedidos = db.Pedidos.Include(p => p.CarritoPedido).Include(p => p.Cliente).Include(p => p.Modo_Pago);
            return View(pedidos.ToList());
        }

        // GET: /Pedido/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            return View(pedido);
        }

        // GET: /Pedido/Create
        public ActionResult Create()
        {
            ViewBag.CarritoPedidoFK = new SelectList(db.Carritos, "ID", "CarritoId");
            ViewBag.ClienteFK = new SelectList(db.Users, "Id", "correo_postal");
            ViewBag.Modo_PagoFK = new SelectList(db.ModosPago, "ID", "nombre_pago");
            return View();
        }

        // POST: /Pedido/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="CarritoPedidoFK,ID,total,Modo_PagoFK,fecha_pedido,iva,ClienteFK")] Pedido pedido)
        {
            if (ModelState.IsValid)
            {
                db.Pedidos.Add(pedido);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CarritoPedidoFK = new SelectList(db.Carritos, "ID", "CarritoId", pedido.CarritoPedidoFK);
            ViewBag.ClienteFK = new SelectList(db.Users, "Id", "correo_postal", pedido.ClienteFK);
            ViewBag.Modo_PagoFK = new SelectList(db.ModosPago, "ID", "nombre_pago", pedido.Modo_PagoFK);
            return View(pedido);
        }

        // GET: /Pedido/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarritoPedidoFK = new SelectList(db.Carritos, "ID", "CarritoId", pedido.CarritoPedidoFK);
            ViewBag.ClienteFK = new SelectList(db.Users, "Id", "correo_postal", pedido.ClienteFK);
            ViewBag.Modo_PagoFK = new SelectList(db.ModosPago, "ID", "nombre_pago", pedido.Modo_PagoFK);
            return View(pedido);
        }

        // POST: /Pedido/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="CarritoPedidoFK,ID,total,Modo_PagoFK,fecha_pedido,iva,ClienteFK")] Pedido pedido)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pedido).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CarritoPedidoFK = new SelectList(db.Carritos, "ID", "CarritoId", pedido.CarritoPedidoFK);
            ViewBag.ClienteFK = new SelectList(db.Users, "Id", "correo_postal", pedido.ClienteFK);
            ViewBag.Modo_PagoFK = new SelectList(db.ModosPago, "ID", "nombre_pago", pedido.Modo_PagoFK);
            return View(pedido);
        }

        // GET: /Pedido/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pedido pedido = db.Pedidos.Find(id);
            if (pedido == null)
            {
                return HttpNotFound();
            }
            return View(pedido);
        }

        // POST: /Pedido/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pedido pedido = db.Pedidos.Find(id);
            db.Pedidos.Remove(pedido);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
