﻿using Compranator.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Compranator.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersAdminController : Controller
    {
        // Constructor vacío para el entity framework
        public UsersAdminController()
        {
        }
        // Constructor que tiene como parámetro el manejador de usuarios y roles, para manipularlos en la tabla de BDD
        public UsersAdminController(AdministradorDeUsuarios userManager, AdministradorDeRoles roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }
        // Para manipular usuarios en la BDD
        private AdministradorDeUsuarios _userManager;
        public AdministradorDeUsuarios UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<AdministradorDeUsuarios>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // Para manipular roles en la BDD
        private AdministradorDeRoles _roleManager;
        public AdministradorDeRoles RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<AdministradorDeRoles>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /UserAdmin/
        public async Task<ActionResult> Index()
        {
            return View(await UserManager.Users.ToListAsync());
        }

        //
        // GET: /UserAdmin/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            ViewBag.RoleNames = await UserManager.GetRolesAsync(user.Id);
            return View(user);
        }

        //
        // GET: /UserAdmin/Create
        public async Task<ActionResult> Create()
        {
            // Obtener la lista de Roles
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }

        //
        // POST: /UserAdmin/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                var user = new Cliente
                {
                    UserName = userViewModel.Email,
                    Email = userViewModel.Email,

                    // Añadir la Dirección Info :
                    correo_postal = userViewModel.correo_postal,
                    municipio = userViewModel.municipio,
                    provincia = userViewModel.provincia,
                    codigo_postal = userViewModel.codigo_postal,
                    fecha_nacimiento = userViewModel.fecha_nacimiento,
                    telefono1 = userViewModel.telefono1,
                    telefono2 = userViewModel.telefono2
                };

                // Añadir la Dirección Info :
                user.correo_postal = userViewModel.correo_postal;
                user.municipio = userViewModel.municipio;
                user.provincia = userViewModel.provincia;
                user.codigo_postal = userViewModel.codigo_postal;
                user.fecha_nacimiento = userViewModel.fecha_nacimiento;
                user.telefono1 = userViewModel.telefono1;
                user.telefono2 = userViewModel.telefono2;

                // Luego crearlo
                var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);

                // Añadir Usuario a los roles seleccionados
                if (adminresult.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First());
                            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First());
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    return View();

                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        //
        // GET: /UserAdmin/Edit/1
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var userRoles = await UserManager.GetRolesAsync(user.Id);

            return View(new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,

                // Añadir la Dirección Info :
                correo_postal = user.correo_postal,
                municipio = user.municipio,
                provincia = user.provincia,
                codigo_postal = user.codigo_postal,
                fecha_nacimiento = user.fecha_nacimiento,
                telefono1 = user.telefono1,
                telefono2 = user.telefono2,
                RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })
            });
        }

        //
        // POST: /UserAdmin/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Email,Id,correo_postal,municipio,provincia,codigo_postal,fecha_nacimiento,telefono1,telefono2")] EditUserViewModel editUser, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null)
                {
                    return HttpNotFound();
                }

                // Añadir la Dirección Info :
                user.UserName = editUser.Email;
                user.Email = editUser.Email;
                user.correo_postal = editUser.correo_postal;
                user.municipio = editUser.municipio;
                user.provincia = editUser.provincia;
                user.codigo_postal = editUser.codigo_postal;
                user.fecha_nacimiento = editUser.fecha_nacimiento;
                user.telefono1 = editUser.telefono1;
                user.telefono2 = editUser.telefono2;

                var userRoles = await UserManager.GetRolesAsync(user.Id);
                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Something failed.");
            return View();
        }

        //
        // GET: /UserAdmin/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /UserAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);
                if (user == null)
                {
                    return HttpNotFound();
                }
                var result = await UserManager.DeleteAsync(user);
                if (!result.Succeeded)
                {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }
    }
}
