﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Compranator.Models;
using System.Web.Services;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Compranator.Controllers
{
    public class ProductoController : Controller
    {
        private CompranatorContexto db = new CompranatorContexto();

        // GET: /Producto/
        public ActionResult Index()
        {
            var productos = db.Productos.Include(p => p.Categoria);
            return View(productos.ToList());
        }

        public JsonResult GetAllProducts()
        {
            using (db)
            {
                var products = db.Productos.Include(p => p.Categoria).Select(i =>
                                            new
                                            {
                                                i.ID,
                                                i.CategoriaFK,
                                                i.Categoria.titulo,
                                                i.modelo,
                                                i.color,
                                                i.talla,
                                                i.tipo_talla,
                                                i.sistemaDeTallas,
                                                i.edades,
                                                i.genero,
                                                i.marca,
                                                i.stock,
                                                i.precio,
                                                i.descripcion,
                                                i.url_image
                                            });

                return Json(products.ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        //
        // GET: /Producto/Browse?categoria=camisa

        public ActionResult Browse(string categoria)
        {
            // Compruebo que recibo el parámetro categoria
            if (categoria == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Recuperar categorias y sus productos asociados de la base de datos
            var generoModelo = db.Categorias.Include("Productos")
                .Single(g => g.titulo == categoria);

            // Compruebo que recibe un producto existente para la categoria
            if (generoModelo == null)
            {
                return HttpNotFound();
            }
            return View(generoModelo);
        }

        // GET: /Producto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: /Producto/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.CategoriaFK = new SelectList(db.Categorias, "ID", "titulo");
            return View();
        }

        // POST: /Producto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,modelo,color,talla,tipo_talla,sistemaDeTallas,edades,genero,marca,stock,precio,descripcion,url_image,CategoriaFK")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file = Request.Files["fileuploadImage"];
                
                string uploadPath = Server.MapPath("~/Content/images/");
                //producto.url_image = string.Concat(uploadPath + file.FileName);
                producto.url_image = file.FileName;
                file.SaveAs(uploadPath + file.FileName);

                db.Productos.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoriaFK = new SelectList(db.Categorias, "ID", "titulo", producto.CategoriaFK);
            return View(producto);
        }

        // GET: /Producto/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoriaFK = new SelectList(db.Categorias, "ID", "titulo", producto.CategoriaFK);
            return View(producto);
        }

        // POST: /Producto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include =
            "ID,modelo,color,talla,tipo_talla,sistemaDeTallas,edades,genero,marca,stock,precio,descripcion,url_image,CategoriaFK")]
            Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaFK = new SelectList(db.Categorias, "ID", "titulo", producto.CategoriaFK);
            return View(producto);
        }

        // GET: /Producto/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: /Producto/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Producto producto = db.Productos.Find(id);
            db.Productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Miniatura(int width, int height)
        {
            /*
             @{var urlImg = @Url.Content("~/Content/images/" + @item.url_image);}
             Para poner en src @Url.Content("~/Content/images/" + @item.url_image)
             @Url.Action("Miniatura", "Producto", new { width = 150, height = 150 })
             */

            // TODO: the filename could be passed as argument of course
            var imageFile = Path.Combine(Server.MapPath("~/Content/images/"), "camisa.png");
            ViewBag.ficherosImagenes = imageFile;
            using (var srcImage = Image.FromFile(imageFile))
            using (var newImage = new Bitmap(width, height))
            using (var graphics = Graphics.FromImage(newImage))
            using (var stream = new MemoryStream())
            {
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawImage(srcImage, new Rectangle(0, 0, width, height));
                newImage.Save(stream, ImageFormat.Png);
                return File(stream.ToArray(), "image/png");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
