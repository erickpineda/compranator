﻿using Compranator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Compranator.Controllers
{
    public class StoreController : Controller
    {
        CompranatorContexto db = new CompranatorContexto();

        //
        // GET: /Store/

        public ActionResult Index()
        {
            var categorias = db.Categorias.ToList();

            return View(categorias);
        }

        //
        // GET: /Store/Browse?categoria=camisa

        public ActionResult Browse(string categoria)
        {
            // Retrieve Genre and its Associated Albums from database
            var genreModel = db.Categorias.Include("Productos")
                .Single(g => g.titulo == categoria);

            return View(genreModel);
        }

        //
        // GET: /Store/Details/5

        public ActionResult Details(int id)
        {
            var producto = db.Productos.Find(id);

            return View(producto);
        }

        //
        // GET: /Store/GenreMenu

        [ChildActionOnly]
        public ActionResult GenreMenu()
        {
            var categoria = db.Categorias.ToList();

            return PartialView(categoria);
        }

    }
}